package it.unikey.interact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InTeractApplication {

	public static void main(String[] args) {
		SpringApplication.run(InTeractApplication.class, args);
	}

}
